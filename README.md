# Prerequisites

If you haven't already:

- Install [the latest version of the JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- Install [git](http://git-scm.com/).
  - On Linux, install git with your OS's package manager.
  - On OS X, installing XCode through the App Store is probably the easiest way to get git.
  - On Windows, there is an installer for git available in the link above.
  - If you want a graphical git tool, consider [SourceTree](https://www.sourcetreeapp.com/) for Mac & Windows. For Linux, there are many options, but [gitg](https://git.gnome.org/browse/gitg) isn't bad.

Now, you can either use this project as is, or learn how to make something like it from scratch yourself.

# I just want to write some Java
## AKA, someone on IRC said to go here

This project will get you up to speed with a grown-up project structure.

Clone the repository:

```
git clone https://marshallpierce@bitbucket.org/marshallpierce/java-quickstart.git your-project-name
```

If you have a git repo set up for your project, use it:
```
git remote origin set-url your-git-repo-url
```

Run the build (use `gradlew.bat` if on windows):
```
./gradlew build
```

Use the Gradle daemon to speed up subsequent builds. In `~/.gradle/gradle.properties`, put this line:
```
org.gradle.daemon = true
```

If you are behind a proxy, you'll also need to add lines to `~/.gradle/gradle.properties` as [described here](https://docs.gradle.org/current/userguide/build_environment.html).


Import the project into IntelliJ. 
- If you have an existing project open, use File > Open and open the build.gradle file.
- If you don't have a project open (e.g. this is your first project), click "Open" in the right side of the "Welcome to IntelliJ IDEA" window.
- When importing, make sure that "use gradle wrapper" is selected.
- IntelliJ should then think for a little while and open your project.

## Making changes from here

You'll probably want to add more classes in `src/main/java` and tests in `src/test/java`. To add dependencies, [include them in your `build.gradle`](https://docs.gradle.org/current/userguide/artifact_dependencies_tutorial.html).

If you make changes to your build.gradle file, make sure to have IntelliJ re-import from gradle by going to the Gradle tab (on the right of the IntelliJ window) and clicking Refresh (the little blue arrows-in-a-circle).

You can always build the project (which includes running the tests) by running `./gradlew build`.

# I want to make this myself

- git init
- gitignore
- download/install gradle
- build.gradle
- gradle wrapper
- source folders
